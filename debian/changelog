parsebib (6.6-1) unstable; urgency=medium

  * New upstream release.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sat, 22 Feb 2025 20:32:06 +0100

parsebib (4.7-1) unstable; urgency=medium

  * New upstream release.
  * Drop patch (not applicable anymore).
  * Bump Standards-Version to 4.7.0 (no changes required).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Wed, 30 Oct 2024 01:17:31 +0100

parsebib (4.3-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Thu, 25 Jul 2024 23:19:39 +0900

parsebib (4.3-1) unstable; urgency=medium

  * New upstream release.
  * d/control: remove Built-Using: field.
  * Add patch to correct version number in parsebib.el.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sun, 09 Oct 2022 04:05:08 +0200

parsebib (4.2-1) unstable; urgency=medium

  * New upstream release.
  * Repair d/watch (using github tags, repairing regexp).

  * d/control:
    - Bump debhelper compat to 13.
    - Add optional Rules-Requires-Root field to satisfy policy.

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Sun, 07 Aug 2022 07:36:21 +0200

parsebib (4.1-1) unstable; urgency=medium

  [ Aymeric Agon-Rambosson ]
  * New upstream release.
  * Adopt the package from Sean Whitton (Closes: #1007867).
  * Update copyright years.
  * Bump standards version to 4.5.1 (removed parallel).

  [ Nicholas D Steeves ]
  * Drop emacs24 from Enhances (package does not exist in bullseye).

 -- Aymeric Agon-Rambosson <aymeric.agon@yandex.com>  Wed, 20 Jul 2022 01:24:42 +0200

parsebib (2.3.1-4) unstable; urgency=medium

  * Team upload

  [ David Krauser ]
  * Update maintainer email address

  [ Dhavan Vaidya ]
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org

  [ David Bremner ]
  * Rebuild with dh-elpa 2.x

 -- David Bremner <bremner@debian.org>  Sun, 24 Jan 2021 07:31:23 -0400

parsebib (2.3.1-3) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 12:42:12 -0300

parsebib (2.3.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with dh-elpa 1.13 to fix byte-compilation with unversioned
    emacs

 -- David Bremner <bremner@debian.org>  Sat, 02 Jun 2018 21:17:31 -0300

parsebib (2.3.1-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years.
  * Bump debhelper compat to 10.
  * Bump standards version to 4.0.0 (no changes required).

 -- Sean Whitton <spwhitton@spwhitton.name>  Mon, 19 Jun 2017 10:30:58 +0100

parsebib (1.0.5-1) unstable; urgency=medium

  * Initial release (Closes: #823887).

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 13 May 2016 12:53:33 -0700
